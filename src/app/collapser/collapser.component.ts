import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, group, sequence } from '@angular/animations';

@Component({
  selector: 'app-collapser',
  templateUrl: './collapser.component.html',
  styleUrls: ['collapser.component.css'],
  animations:[

  ]
})
export class CollapserComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
